import React from "react";
import { Router, Switch, Route, Link } from "react-router-dom";
import "./pieces/IndependentComponents/assets/styles.css";
import "./pieces/IndependentComponents/assets/mdb.css";
import Navbar from "./pieces/IndependentComponents/Navbar";
import Footer from "./pieces/IndependentComponents/Footer";
import Philosophy from "./pieces/Philosophy";
import Template from "./pieces/Template";
import Resume from "./pieces/Resume";
import Home from "./pieces/Home";
import Contact from "./pieces/Contact";
import Drawings from './pieces/Drawings'

const Routes = () => {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/philosophy" exact component={Philosophy} />
        <Route path="/resume" exact component={Resume} />
        <Route path="/contact" exact component={Contact} />
      </Switch>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <Footer />
    </div>
  );
};

export default Routes;
