import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter,
  Redirect,
  NavLink,
  matchPath,
  Link,
  BrowserRouter
} from "react-router-dom";
import ReactDOM from "react-dom";
import "./pieces/IndependentComponents/assets/styles.css";
import "./pieces/IndependentComponents/assets/mdb.css";
import "./pieces/IndependentComponents/assets/mdb.css";
import Navbar from "./pieces/IndependentComponents/Navbar";
import Footer from "./pieces/IndependentComponents/Footer";
import Philosophy from "./pieces/Philosophy";
import Resume from "./pieces/Resume";
import Home from "./pieces/Home";
import Contact from "./pieces/Contact";
import Drawings from './pieces/Drawings'
const Routes = () => {
  return (
    <div className="App">
     
      <Router>
       <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/philosophy" component={withRouter(Philosophy)} />
          <Route path="/drawings" component={withRouter(Drawings)} />
          <Route path="/resume" component={withRouter(Resume)} />
          <Route path="/contact" component={withRouter(Contact)} />
        </Switch>
      </Router>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <Footer />
    </div>
  );
};

const rootElement = document.getElementById("root");
ReactDOM.render(
  <BrowserRouter>
    <Routes />
  </BrowserRouter>,
  rootElement
);
