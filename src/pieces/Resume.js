import React from "react";
import "./IndependentComponents/assets/styles.css";
import "./IndependentComponents/assets/mdb.css";
import Card from "./IndependentComponents/Card";
import countries from './IndependentComponents/images/countries.png'
import Trello from './IndependentComponents/images/Trello.png'
import Contacts from './IndependentComponents/images/Contacts.png'
import countries2 from './IndependentComponents/images/countries2.png'
import feedback from './IndependentComponents/images/feedback.png'
import reports from './IndependentComponents/images/reports.png'
import Trivia from './IndependentComponents/images/Trivia.png'


const Projects = () => {
  return (
    <div className="container">
      <div className="row">
      
        <div className="col-md">
        <a href="https://codesandbox.io/s/countries-m06by" target="_blank">
          <Card
            img={countries2}
            title="Countries"
            text="Skills: Retrieving Data and Images from APIs, Altering Data from Servers"
            htitle="React"
          />
          </a>
        </div>
        <div className="col-md">
        <a href="https://codesandbox.io/s/github/lezzles11/Phonebook" target="_blank">
          <Card
            img={Contacts}
            title="Contacts"
            text="Skills: Using Effect Hooks (React), Deploying in Heroku, Configuring Middleware, Routing using Express"
            htitle="React"
          />
          </a>
        </div>
      </div>

      <div className="row">
        <div className="col-md">
        <a href="https://tj9od.codesandbox.io/" target="_blank">
          <Card 
          img={Trello}
          title="Trello" 
          text="Managing States and Refactoring Modules" 
          htitle="React"  />
          </a>
        </div>


        <div className="col-md">
        <a href="https://github.com/lezzles11/Progress_Reports" target="_blank">
          <Card
            img={reports}
            title="Progress Reports"
            text="Skills: Using ArrayList and HashMap Data Structures as well as using a variety of Java classes and methods"
            htitle="Java"
          />
          </a>
        </div>
      </div>

      <div className="row">
        <div className="col-md">
         <a target="_blank" href="https://codesandbox.io/s/feedback-9wjpo">
          <Card
            img={feedback}
            title="Feedback System"
            text="Skills: Managing Components, Destructuring Variables and Passing States to Child Components"
            htitle="React"
          />
          </a>
        </div>
       
        <div className="col-md">
           <a target="_blank" href="https://codepen.io/lezzles11/pen/xoMZaj">
          <Card
            img={Trivia}
            title="Trivia"
            text="Skills: Using Javascript Functions and Objects"
            htitle="Javascript"
          />
           </a>
        </div>
       
      </div>


    </div>
  );
};
export default Projects;
