import React from "react";
import { Link } from "react-router-dom";
import "./assets/styles.css";
import "./assets/mdb.css";
import "./assets/grid.css";
import Logo from "./Logo";

const Navbar = props => {
  const style = {
    color: "white",
    padding: "10px"
  };
  return (
    <nav className="navbar navbar-toggle fixed-top navbar-expand-lg navbar-dark pink scrolling-navbar">
      <Logo />
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li style={style} className="nav-item">
            <Link className="nav-link" to="/">
              Home
            </Link>
            <span className="sr-only" />
          </li>
          <li style={style} className="nav-item">
            <Link className="nav-link" to="/philosophy">
              Projects 
            </Link>
          </li>


          <li style={style} className="nav-item">
            <Link className="nav-link" to="/resume">
              Resume
            </Link>
          </li>
          
          <li style={style} className="nav-item">
            <Link className="nav-link" to="/contact">
              Contact
            </Link>
          </li>
        </ul>
        <ul className="navbar-nav nav-flex-icons">
          <li className="nav-item">
            <a
              target="_blank"
              className="nav-link"
              href="https://lezzles11.monday.com/boards/224665141/"
            >
              <i className="fas fa-tasks" />
            </a>
          </li>
          <li className="nav-item">
            <a
              target="_blank"
              className="nav-link"
              href="https://github.com/lezzles11"
            >
              <i className="fab fa-github" />
            </a>
          </li>
          <li className="nav-item">
            <a
              target="_blank"
              className="nav-link"
              href="https://www.linkedin.com/in/lesley-cheung-b375876b/"
            >
              <i className="fab fa-linkedin-in" />
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
