import React, { Component } from "react";
import "./assets/styles.css";
import "./assets/mdb.css";
import "./assets/grid.css";

const Header = props => {
  const headerStyle = {
    textShadow: "2px 2px crimson"
  };
  const subHeaderStyle = {
    textShadow: "1.3px 1.3px black"
  };
  const block = {
    color: "white",
    margin: "auto",
    display: "block",
    padding: "20px",
    textAlign: "center"
  };
  return (
    <div className="wholeRow" style={block}>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <h1 className="white-text" style={headerStyle}>
        {props.header}
      </h1>
      <h3 className="white-text">
        here is how I became a{" "}
        <strong id="gradienthome" style={subHeaderStyle}>
          software engineer
        </strong>
      </h3>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
};
export default Header;
