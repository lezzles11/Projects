import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./assets/styles.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandLizard } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
const Logo = () => {
  const logoStyle = {
    fontFamily: "Libre Baskerville",
    fontSize: "1em",
    lineHeight: "0%",
    padding: 0,
    margin: 0,
    textAlign: "right"
  };
  const smallFont = {
    fontSize: "0.6em",
    lineHeight: "0%",
    right: 0,
    padding: 0,
    margin: 7
  };
  return (
    <div style={logoStyle}>
      <Link className="navbar-brand" to="/">
        <p style={smallFont}> {"   "}hand coded by</p>
        <p style={logoStyle}>
          <FontAwesomeIcon icon={faHandLizard} /> Lesley the Software Engineer
        </p>
      </Link>
    </div>
  );
};
export default Logo;
