import React from "react";
import "./assets/styles.css";
import "./assets/mdb.css";
import "./assets/grid.css";
import Logo from "./Logo";

const Footer = props => {
  const style = {
    position: "fixed",
    bottom: 0,
    color: "white",
    textAlign: "right",
    right: 0,
    padding: "10px"
  };
  return (
    <div className="container" style={style}>
      <Logo /> {props.footer}
    </div>
  );
};
export default Footer;
