import React from "react";
import "./assets/card.css";
import "./assets/mdb.css";

const BCard = ({ img, title, text }) => {
  const imgStyle = {
    height: "200px",
    border: "1px solid #ddd",
    padding: "5px",
    width: "auto",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
  }
  return (
    <div className="card">
      <div className="view overlay">
        <img
          className="card-img-top"
          alt="bootstrap"
          style={imgStyle}
          src={img}
        />
        <a href="index.html">
          <div className="mask rgba-white-slight" />
        </a>
      </div>
      <div className="card-body">
        <h4 className="card-title">{title}</h4>
        <p className="card-text">{text}</p>
      </div>
    </div>
  );
};
const Card = ({ img, title, text, htitle, htext }) => {
  const textStyle = {
    color: "darkBlue"
  };
  return (
    <div className="cardContainer ">
      <div className="content">
        {/* a link supposed to b here*/}
        <div className="content-overlay" />
        <BCard className="content-image" img={img} title={title} text={text} />
        <div className="content-details fadeIn-left">
          <h3 style={textStyle}>{htitle}</h3>
          <p>{htext}</p>
        </div>
        
      </div>
    </div>
  );
};

export default Card;
