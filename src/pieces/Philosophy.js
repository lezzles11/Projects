import React from "react";
import "./IndependentComponents/assets/styles.css";
import "./IndependentComponents/assets/mdb.css";
import "./IndependentComponents/assets/grid.css";
import Header from "./IndependentComponents/Header";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Projects from "./Projects";
const Philosophy = () => {
  return (
    <div className="container">
      <Header header="Projects" />
      <Projects />
    </div>
  );
};

export default Philosophy;