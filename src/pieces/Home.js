import React from "react";
import "./IndependentComponents/assets/styles.css";
import "./IndependentComponents/assets/mdb.css";
import "./IndependentComponents/assets/grid.css";
import Header from "./IndependentComponents/Header";

const Graph = () => {
  return (
    <div>
      {" "}
      <iframe
        width="100%"
        height="650"
        src="https://jsfiddle.net/lezzles11/uwgzhL3s/74/embedded/result"
        webkitallowfullscreen="true"
        mozallowfullscreen
        allowFullScreen
        frameborder="0"
      />
    </div>
  );
};

const Home = () => {
  return (
    <div>
      <Header header="Hello 👋" />
      <Graph />
    </div>
  );
};
export default Home;
