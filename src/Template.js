import React from "react";
import "./pieces/IndependentComponents/assets/styles.css";
import "./pieces/IndependentComponents/assets/mdb.css";
import "./pieces/IndependentComponents/assets/grid.css";
import Navbar from "./pieces/IndependentComponents/Navbar";
import Header from "./pieces/IndependentComponents/Header";
import Footer from "./pieces/IndependentComponents/Footer";
import Resume from "./pieces/Resume";
import Drawings from './pieces/Drawings'

const Template = () => {
  return (
    <div className="App">
      <Navbar />
      <Header header="Template" />
      <Skills />
      <Footer />
    </div>
  );
};

export default Template;
